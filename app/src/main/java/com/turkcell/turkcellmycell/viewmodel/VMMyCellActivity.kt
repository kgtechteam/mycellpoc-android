package com.turkcell.turkcellmycell.viewmodel

import androidx.lifecycle.ViewModel
import javax.inject.Inject

/**
 * Created by MuratCan on 2019-12-12.
 */
class VMMyCellActivity @Inject constructor() : ViewModel()