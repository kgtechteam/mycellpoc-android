package com.turkcell.turkcellmycell.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.turkcell.turkcellmycell.di.key.ViewModelKey
import com.turkcell.turkcellmycell.ViewModelFactory
import com.turkcell.turkcellmycell.viewmodel.*
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

/**
 * Created by MuratCan on 2019-11-11.
 */
@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @IntoMap
    @Binds
    @ViewModelKey(VMMyCellActivity::class)
    abstract fun provideVMMyCellActivity(ActivityViewModel: VMMyCellActivity): ViewModel

    @IntoMap
    @Binds
    @ViewModelKey(VMHomeFragment::class)
    abstract fun provideVMHomeFragment(HomeViewModel: VMHomeFragment): ViewModel

    @IntoMap
    @Binds
    @ViewModelKey(VMMessageFragment::class)
    abstract fun provideVMMessageFragment(MessageViewModel: VMMessageFragment): ViewModel

    @IntoMap
    @Binds
    @ViewModelKey(VMNotificationsFragment::class)
    abstract fun provideVMNotificationsFragment(NotificationsViewModel: VMNotificationsFragment): ViewModel

    @IntoMap
    @Binds
    @ViewModelKey(VMPaymentFragment::class)
    abstract fun provideVMPaymentFragment(PaymentViewModel: VMPaymentFragment): ViewModel

    @IntoMap
    @Binds
    @ViewModelKey(VMSearchFragment::class)
    abstract fun provideVMSearchFragment(SearchViewModel: VMSearchFragment): ViewModel

}