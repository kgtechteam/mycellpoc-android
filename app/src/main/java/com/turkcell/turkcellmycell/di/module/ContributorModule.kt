package com.turkcell.turkcellmycell.di.module

import com.turkcell.turkcellmycell.MyCellActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ContributorModule {

    /**
    Defines which fragments will be used by [MyCellActivity]
     */
    @ContributesAndroidInjector(modules = [FragmentContributorModule::class])
    abstract fun contributeMyCellActivity(): MyCellActivity
}