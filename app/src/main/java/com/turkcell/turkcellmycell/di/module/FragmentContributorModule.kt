package com.turkcell.turkcellmycell.di.module

import com.turkcell.turkcellmycell.di.scope.FragmentScope
import com.turkcell.turkcellmycell.ui.view.home.HomeFragment
import com.turkcell.turkcellmycell.ui.view.message.MessageFragment
import com.turkcell.turkcellmycell.ui.view.notification.NotificationsFragment
import com.turkcell.turkcellmycell.ui.view.payment.PaymentFragment
import com.turkcell.turkcellmycell.ui.view.search.SearchFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

/**
 * [FragmentContributorModule] is used inside [ContributorModule]
 * With @ContributesAndroidInjector(modules = MyCellFragmentContributorModule.class)
 * defines which module will be used to inject objects to declared fragments
 */
@Module
abstract class FragmentContributorModule {

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeHomeFragment(): HomeFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeMessageFragment(): MessageFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeNotificationsFragment(): NotificationsFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributePaymentFragment(): PaymentFragment

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributeSearchFragment(): SearchFragment


}