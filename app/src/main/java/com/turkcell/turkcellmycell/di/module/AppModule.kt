package com.turkcell.turkcellmycell.di.module

import android.app.Application
import android.content.Context
import com.turkcell.turkcellmycell.di.qualifiers.ApplicationContextQualifier
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by MuratCan on 2019-12-12.
 */

@Module
class AppModule {

    @Provides
    @Singleton
    @ApplicationContextQualifier
    fun provideContext(application: Application): Context {
        return application.applicationContext
    }
}