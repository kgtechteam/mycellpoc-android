package com.turkcell.turkcellmycell.di.component

import android.app.Application
import com.turkcell.turkcellmycell.MyCellApplication
import com.turkcell.turkcellmycell.di.module.AppModule
import com.turkcell.turkcellmycell.di.module.ContributorModule
import com.turkcell.turkcellmycell.di.module.ViewModelModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import javax.inject.Singleton

/**
 * Created by MuratCan on 2019-12-12.
 */

@Singleton
@Component(
    modules = [AndroidInjectionModule::class,
    AppModule::class,
    ViewModelModule::class,
    ContributorModule::class
    ]
)

interface AppComponent: AndroidInjector<MyCellApplication> {

    @Component.Builder
    interface Builder{
        @BindsInstance
        fun application(application: Application): Builder

        fun build(): AppComponent
    }

    override fun inject(instance: MyCellApplication?)
}