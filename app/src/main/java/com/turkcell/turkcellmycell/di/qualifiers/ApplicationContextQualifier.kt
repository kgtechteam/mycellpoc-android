package com.turkcell.turkcellmycell.di.qualifiers

import javax.inject.Qualifier

@Qualifier
annotation class ApplicationContextQualifier