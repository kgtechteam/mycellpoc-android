package com.turkcell.turkcellmycell.util

import android.app.Activity
import java.io.IOException
import java.nio.charset.Charset

/**
 * Created by MuratCan on 2019-12-13.
 */

fun Activity.loadJSONFromAsset(jsonFileName: String): String {
    val json: String?
    try {
        val conn = this.assets.open(jsonFileName)
        val size = conn.available()
        val buffer = ByteArray(size)
        conn.read(buffer)
        conn.close()
        json = String(buffer, Charset.forName("UTF-8"))
    } catch (ex: IOException) {
        ex.printStackTrace()
        return ""
    }
    return json
}