package com.turkcell.turkcellmycell.ui.view.notification

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.turkcell.turkcellmycell.R
import com.turkcell.turkcellmycell.databinding.FragmentNotificationsBinding
import com.turkcell.turkcellmycell.ui.base.BaseFragment
import com.turkcell.turkcellmycell.viewmodel.VMNotificationsFragment

class NotificationsFragment : BaseFragment<VMNotificationsFragment, FragmentNotificationsBinding>() {
    override fun getViewModel(): Class<VMNotificationsFragment> = VMNotificationsFragment::class.java

    override fun getLayoutRes(): Int = R.layout.fragment_notifications

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding.viewModel = viewModel

        return binding.root
    }

}
