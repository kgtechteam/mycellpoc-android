package com.turkcell.turkcellmycell.ui.view.payment

import android.app.Activity
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import com.turkcell.turkcellmycell.R
import com.turkcell.turkcellmycell.databinding.FragmentPaymentBinding
import com.turkcell.turkcellmycell.model.Product
import com.turkcell.turkcellmycell.model.ProductsItem
import com.turkcell.turkcellmycell.ui.adapter.PaymentAdapter
import com.turkcell.turkcellmycell.ui.base.BaseFragment
import com.turkcell.turkcellmycell.util.loadJSONFromAsset
import com.turkcell.turkcellmycell.viewmodel.VMPaymentFragment
import hari.bounceview.BounceView
import kotlinx.android.synthetic.main.toolbar_payment.view.*

class PaymentFragment : BaseFragment<VMPaymentFragment, FragmentPaymentBinding>() {

    override fun getViewModel(): Class<VMPaymentFragment> = VMPaymentFragment::class.java
    override fun getLayoutRes(): Int = R.layout.fragment_payment

    private lateinit var adapter: PaymentAdapter
    private val gson = Gson()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding.viewModel = viewModel


        adapter = PaymentAdapter()
        activity?.let {
            adapter.setPostList(fetchProductList(it))
        }
        with(binding){
            recyclerview.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            recyclerview.adapter = adapter

        }

        addButtonAnimations()
        return binding.root
    }

    private fun fetchProductList(activity: Activity): List<Product>{
        val productItem =
            gson.fromJson<ProductsItem>(
                activity.loadJSONFromAsset("mockData.json"),
                ProductsItem::class.java
            )
        return productItem.products
    }

    private fun addAnim(v: View){
        BounceView.addAnimTo(v).setScaleForPopOutAnim(1f, 1f)
    }

    private fun addButtonAnimations(){
        with(binding.paymentToolbar){
            addAnim(button1)
            addAnim(button2)
            addAnim(button3)
            addAnim(button4)
            addAnim(button5)
        }
    }
}
