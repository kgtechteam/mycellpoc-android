package com.turkcell.turkcellmycell.ui.view.search

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.turkcell.turkcellmycell.R
import com.turkcell.turkcellmycell.databinding.FragmentSearchBinding
import com.turkcell.turkcellmycell.ui.base.BaseFragment
import com.turkcell.turkcellmycell.viewmodel.VMSearchFragment

class SearchFragment : BaseFragment<VMSearchFragment, FragmentSearchBinding>() {

    override fun getViewModel(): Class<VMSearchFragment> = VMSearchFragment::class.java
    override fun getLayoutRes(): Int = R.layout.fragment_search

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding.viewModel = viewModel


        return binding.root
    }

}
