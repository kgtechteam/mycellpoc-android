package com.turkcell.turkcellmycell.ui.view.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import com.turkcell.turkcellmycell.R
import com.turkcell.turkcellmycell.databinding.FragmentHomeBinding
import com.turkcell.turkcellmycell.ui.adapter.HomeAdapter
import com.turkcell.turkcellmycell.ui.base.BaseFragment
import com.turkcell.turkcellmycell.viewmodel.VMHomeFragment

class HomeFragment : BaseFragment<VMHomeFragment, FragmentHomeBinding>(), HomeAdapter.ItemClickListener{

    override fun getViewModel(): Class<VMHomeFragment> = VMHomeFragment::class.java
    override fun getLayoutRes(): Int = R.layout.fragment_home

    private lateinit var adapter: HomeAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding.viewModel = viewModel

        adapter = HomeAdapter(this)
        with(binding){
            recyclerview.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            recyclerview.adapter = adapter

        }

        return binding.root
    }

    override fun onItemClick(view: View, position: Int, title: String) {
        Toast.makeText(context, "$title clicked", Toast.LENGTH_SHORT).show()
    }

}
