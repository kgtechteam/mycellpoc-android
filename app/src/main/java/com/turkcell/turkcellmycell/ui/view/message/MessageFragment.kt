package com.turkcell.turkcellmycell.ui.view.message

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.turkcell.turkcellmycell.R
import com.turkcell.turkcellmycell.databinding.FragmentMessageBinding
import com.turkcell.turkcellmycell.ui.base.BaseFragment
import com.turkcell.turkcellmycell.viewmodel.VMMessageFragment

class MessageFragment : BaseFragment<VMMessageFragment, FragmentMessageBinding>() {

    override fun getViewModel(): Class<VMMessageFragment> = VMMessageFragment::class.java
    override fun getLayoutRes(): Int = R.layout.fragment_message

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        binding.viewModel = viewModel

        return binding.root
    }
}
