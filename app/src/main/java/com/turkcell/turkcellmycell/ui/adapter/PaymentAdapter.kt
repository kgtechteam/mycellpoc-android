package com.turkcell.turkcellmycell.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.turkcell.turkcellmycell.R
import com.turkcell.turkcellmycell.model.Product
import hari.bounceview.BounceView
import kotlinx.android.synthetic.main.row_payment_list.view.*

/**
 * Created by MuratCan on 2019-12-13.
 */
class PaymentAdapter : RecyclerView.Adapter<PaymentAdapter.PaymentAdapterViewHolder>() {

    var list: MutableList<Product> = mutableListOf()
    private var lastPosition = -1

    fun addPostList(items: List<Product>) {
        val beforeSize = list.size
        list.addAll(items)
        notifyItemRangeInserted(beforeSize, list.size)
    }

    fun setPostList(items: List<Product>) {
        list.clear()
        list.addAll(items)
        notifyDataSetChanged()
    }

    fun removeItem(pos: Int) {
        list.removeAt(pos)
        notifyItemRemoved(pos)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PaymentAdapterViewHolder {
        return PaymentAdapterViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.row_payment_list, parent, false))
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun onBindViewHolder(holder: PaymentAdapterViewHolder, position: Int) {
        val mItem = list[position]
        holder.setData(mItem)
        setAnimation(holder.itemView, position)
    }

    private fun setAnimation(viewToAnimate: View, position: Int) {
        if (position > lastPosition) {
            val animation = AnimationUtils.loadAnimation(viewToAnimate.context, android.R.anim.slide_in_left)
            animation.duration = 350
            viewToAnimate.startAnimation(animation)
            lastPosition = position
        }
    }

    inner class PaymentAdapterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun setData(item: Product) {
            with(itemView) {
                title_product.text = item.product_name
                right_title_product.text = item.product_apps
                titleInternet.text = item.product_internet
                title_min.text = item.product_call
                total_text.text = item.product_price

                BounceView.addAnimTo(button).setScaleForPopOutAnim(1f, 0f)

                setGradients(itemView)
            }
        }

        private fun setGradients(v: View){
            with(v){
                when(adapterPosition){
                    0 -> {
                        bottom_gradient.background = ContextCompat.getDrawable(context, R.drawable.gradient_first)
                        right_title_product_frame.background = ContextCompat.getDrawable(context, R.drawable.gradient_right_first)
                    }
                    1 -> {
                        bottom_gradient.background = ContextCompat.getDrawable(context, R.drawable.gradient_second)
                        right_title_product_frame.background = ContextCompat.getDrawable(context, R.drawable.gradient_right_second)

                    }
                    2 -> {
                        bottom_gradient.background = ContextCompat.getDrawable(context, R.drawable.gradient_third)
                        right_title_product_frame.background = ContextCompat.getDrawable(context, R.drawable.gradient_right_third)

                    }
                }
            }
        }

    }

}