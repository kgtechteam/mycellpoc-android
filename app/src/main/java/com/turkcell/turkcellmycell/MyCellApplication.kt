package com.turkcell.turkcellmycell

import android.content.Context
import com.turkcell.turkcellmycell.di.component.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

/**
 * Created by MuratCan on 2019-12-12.
 */

class MyCellApplication : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.builder().application(this).build()
    }

    init {
        instance = this
    }

    companion object {
        private var instance = MyCellApplication()
        val context: Context
            get() = instance.applicationContext
    }

}