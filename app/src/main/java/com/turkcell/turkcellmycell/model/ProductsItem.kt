package com.turkcell.turkcellmycell.model

data class ProductsItem(
    val products: List<Product>
)